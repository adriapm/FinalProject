package main.principal.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

import main.principal.Actors.Coin;
import main.principal.Actors.Skills.Fireball;
import main.principal.Actors.GhostNpc;
import main.principal.Actors.Pumpkin;
import main.principal.Actors.Wizzard;
import main.principal.Principal;
import main.principal.Tools.WorldContactListener;

/**
 * Created by adriapm on 07/05/18.
 */
//Tots els mapes hereden de BaseScreen
public class PrimerMapa extends BaseScreen {
    //creem un Escenari ( stage ) on posarem tots els elements del mapa
    private Stage stage;
    private Pumpkin player;
    private GhostNpc ghost;
    private World world;
    //per fer les colisiones amb objectes del map;
    private Box2DDebugRenderer b2dr;

    private TiledMap map;
    private OrthogonalTiledMapRenderer renderer;
    private Hud hud;
    public OrthographicCamera camera;



    public static Array<Coin> coins=new Array<Coin>();
    public static Array<Body> bCoins=new Array<Body>();


    public static Array<Fireball> fireballs=new Array<Fireball>();


    public PrimerMapa(Principal game) {
        super(game);
        camera = new OrthographicCamera();
        map = new TmxMapLoader().load("maps/map1/map_1.tmx");
        renderer = new OrthogonalTiledMapRenderer(map, 1);

        world = new World(new Vector2(0, -10f), true);
        b2dr = new Box2DDebugRenderer();
        hud = game.hud;

        //per introduir una camara
        stage = new Stage();
        stage.setViewport(new ExtendViewport(640, 480));
        stage.getCamera().position.set(10f, 10f, 1f);
        stage.getViewport().update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        actorsSetters();
        loadPlayer();
        ghost = new GhostNpc(player);

        mapBodyBuilder();
        afegirActorsAlStage();

        world.setContactListener(new WorldContactListener(this));
        Hud.setLevel(1);
    }




    public void loadPlayer(){
        int save = checkSavePont();
        //xd: y 1381.515
        //xd: x 1903.8042
        if (save == 0) {
            player = new Pumpkin(1000, 2550);
        } else if (save == 1) {
            player = new Pumpkin(1903.8042f*2, 1381.515f*2);
        } else if(save==2){
            player = new Pumpkin(2720 * 2, 1158 * 2);
        }
    }

    public void actorsSetters(){
        Pumpkin.setStage2(stage);
        Pumpkin.setWorld(world);
        Wizzard.setWorld(world);
        Coin.setWorld(world);
        Coin.setStage2(stage);
        Wizzard.setStage2(stage);
        GhostNpc.setWorld(world);
        GhostNpc.setStage2(stage);
    }

    public void mapBodyBuilder(){

        BodyDef bdef = new BodyDef();
        PolygonShape shape = new PolygonShape();
        FixtureDef fdef = new FixtureDef();
        Body body;

        for (MapObject object : map.getLayers().get("GroundObject").getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();
            bdef.type = BodyDef.BodyType.StaticBody;
            bdef.position.set(rect.getX() + rect.getWidth() / 2, rect.getY() + rect.getHeight() / 2);
            body = world.createBody(bdef);
            shape.setAsBox(rect.getWidth() / 2, rect.getHeight() / 2);
            fdef.shape = shape;
            body.createFixture(fdef);
        }
        for (MapObject object : map.getLayers().get("Spikes").getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();
            bdef.type = BodyDef.BodyType.StaticBody;
            bdef.position.set(rect.getX() + rect.getWidth() / 2, rect.getY() + rect.getHeight() / 2);
            body = world.createBody(bdef);
            shape.setAsBox(rect.getWidth() / 2, rect.getHeight() / 2);
            fdef.shape = shape;
            body.createFixture(fdef).setUserData("spike");
        }

        for (MapObject object : map.getLayers().get("LayerOfWizzard").getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();
            bdef.type = BodyDef.BodyType.StaticBody;
            bdef.position.set(rect.getX() + rect.getWidth() / 2, rect.getY() + rect.getHeight() / 2);
            body = world.createBody(bdef);
            shape.setAsBox(rect.getWidth() / 2, rect.getHeight() / 2);
            fdef.shape = shape;
            body.createFixture(fdef);
            Wizzard x = new Wizzard(player, (rect.getX() + rect.getWidth() / 2) * 2, (rect.getY() + rect.getHeight() / 2) * 2 + 65);
            stage.addActor(x);
        }
        int nCoin=0;
        for (MapObject object : map.getLayers().get("Coin").getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();
            bdef.type = BodyDef.BodyType.StaticBody;
            bdef.position.set(rect.getX() + rect.getWidth(), rect.getY() + rect.getHeight() + 4);
            body = world.createBody(bdef);
            shape.setAsBox(rect.getWidth() / 2, (rect.getHeight() / 2));
            fdef.shape = shape;
            body.createFixture(fdef).setUserData("coin"+nCoin);
            nCoin++;
            Coin x = new Coin((rect.getX() + rect.getWidth() / 2) * 2, (rect.getY() + rect.getHeight() / 2) * 2);
            coins.add(x);
            stage.addActor(x);
        }
        for (MapObject object : map.getLayers().get("Ceiling").getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();
            bdef.type = BodyDef.BodyType.StaticBody;
            bdef.position.set(rect.getX() + rect.getWidth() / 2, rect.getY() + rect.getHeight() / 2);
            body = world.createBody(bdef);
            shape.setAsBox(rect.getWidth() / 2, rect.getHeight() / 2);
            fdef.shape = shape;
            body.createFixture(fdef).setUserData("Ceiling");
        }
        shape.dispose();

    }



    @Override
    public void show() {
        super.show();
    }


    @Override
    public void hide() {
        super.hide();
    }


    public void gameInit() {
        ghost.setGhostX(0);
        ghost.setGhostY(2600);

        if (checkSavePont() == 0) {
            player.init(1000,2550);
        } else if (checkSavePont() == 1) {
            player.init(1903.8042f*2, 1381.515f*2);
        } else if(checkSavePont()==2){
            player.init(2720 * 2, 1158 * 2);
        }

    }

    //El codi de render s'executa 60 cops per segon
    @Override
    public void render(float delta) {
        super.render(delta);

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        //stage act crida la rutina act de cada actor
        stage.act();

        //perque la camara estigui amb el player

       // world.step(1 / 60f, 6, 2);
        world.step(delta, 6, 2);

        renderer.setView((OrthographicCamera) stage.getCamera());
        renderer.render();

        //b2dr.render(world, stage.getCamera().combined);

        if (Pumpkin.getCurrentState().equals(Pumpkin.State.DEAD)) {
            game.setScreen(game.deadScreen);
            Hud.sumDeathCount();
            Hud.restartTime();
            game.stopMusic();
            game.deathSound();
        }

        stage.draw();
        game.batch.setProjectionMatrix(hud.stage.getCamera().combined);
        hud.stage.draw();
        hud.update();

        updateSavePont();
        completedLevel();

        for (Body c : bCoins) {
            world.destroyBody(c);
        }
        bCoins.clear();

        for (Fireball c : fireballs) {
            world.destroyBody(c.getBody());
        }
        fireballs.clear();

        Gdx.app.log("xd","x "+player.getX());
        Gdx.app.log("xd","y "+player.getY());

    }

    public void destroyFireball(Fireball fb) {
        fireballs.add(fb);
    }

    public void destroyCoin(String coin, Body bd){
        int x=Integer.valueOf(coin.substring(4));
        coins.get(x).dispose();
        coins.get(x).setVisible(false);
        bCoins.add(bd);
    }



    public void completedLevel(){

        if(player.getY()==1294.3949f && player.getX()>4237.764f){
            FileWriter file = null;
            PrintWriter pw = null;
            try {
                file = new FileWriter("level.txt");
                pw = new PrintWriter(file);
                pw.println(2);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (null != file)
                        file.close();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            game.stopMusic();
            game.winSound();
            Hud.endLevelScore();
            game.setScreen(game.stage2);
        }
    }




    private void updateSavePont() {


        if (player.getY() == 1157.6849f) {
            int save = checkSavePont();
            if (save == 0 || save == 1) {
                FileWriter file = null;
                PrintWriter pw = null;
                try {
                    file = new FileWriter("save.txt");
                    pw = new PrintWriter(file);
                    pw.println(2);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (null != file)
                            file.close();
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            }
        }else if(player.getX()<1944.3579f && player.getX()>1865.348f &&player.getY()==1381.515f){
            int save = checkSavePont();
            if (save == 0 || save == 2) {
                FileWriter file = null;
                PrintWriter pw = null;
                try {
                    file = new FileWriter("save.txt");
                    pw = new PrintWriter(file);
                    pw.println(1);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (null != file)
                            file.close();
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            }
        }

    }




    private int checkSavePont() {

        File file = null;
        FileReader fr = null;
        BufferedReader br = null;
        int x = 0;
        try {
            file = new File("save.txt");
            fr = new FileReader(file);
            br = new BufferedReader(fr);

            String line;
            while ((line = br.readLine()) != null) {
                x = Integer.valueOf(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return x;
    }




    //Afegim actors al escenari
    private void afegirActorsAlStage() {
        stage.addActor(ghost);
        stage.addActor(player);
    }

    //Lliberem recursos
    @Override
    public void dispose() {
        stage.clear();
        player.dispose();
        world.dispose();
//        ghost.dispose();
        renderer.dispose();
        map.dispose();


        for (Fireball c : fireballs) {
            c.dispose();
        }
        fireballs.clear();
    }

}
