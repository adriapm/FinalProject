package main.principal.Screens;

import main.principal.Actors.Coin;
import main.principal.Principal;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.scenes.scene2d.Stage;

/**
 * Created by adriapm on 14/05/18.
 */

public class Proves extends BaseScreen {

    private Box2DDebugRenderer debugRenderer;
    private World world;
    private OrthographicCamera camera;
    private Body body, sueloBody;
    private Fixture fixture;
    private Coin coin;
    private Stage stage;


    public Proves(Principal game) {
        super(game);
        stage = new Stage();
        debugRenderer = new Box2DDebugRenderer();
        /*world = new World(new Vector2(0, -10), true);
        camera = new OrthographicCamera(32,18);
        BodyDef bd = createBd();
        body = world.createBody(bd);

       // sueloBody = world.createBody()
        PolygonShape ps = new PolygonShape();
        ps.setAsBox(1,1);
        fixture = body.createFixture(ps,1);
        ps.dispose();*/


    }

    private BodyDef createBd() {
        BodyDef bd = new BodyDef();
        bd.position.set(0,10);
        bd.type = BodyDef.BodyType.DynamicBody;
        return bd;
    }


    @Override
    public void render(float delta) {


        // You know the rest...
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
     /*   world.step(delta,6,2);
        camera.update();
        debugRenderer.render(world, camera.combined);
        if(Gdx.input.isKeyJustPressed(Input.Keys.S)) {

            body.applyLinearImpulse(0, 20, 0, 0, true);

            Gdx.app.log("EEE", "EEE");
        }
        if(Gdx.input.isKeyJustPressed(Input.Keys.D)) {
            //setPosition();

            body.applyLinearImpulse(20, 20, 0, 0, true);

            Gdx.app.log("EEE", "EEE");
        }
        if(Gdx.input.isKeyJustPressed(Input.Keys.A)) {

            body.applyLinearImpulse(-20, 20, 0, 0, true);

            Gdx.app.log("EEE", "EEE");
        }*/
        stage.act();
        stage.draw();

        }

    @Override
    public void dispose() {
        // Hey, I actually did some clean up in a code sample!
        world.dispose();
        debugRenderer.dispose();
        stage.dispose();
    }
}
