package main.principal.Screens;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

import main.principal.Principal;

/**
 * Created by adriapm on 18/05/18.
 */



public class ScreenTransition2 extends BaseScreen {

    public Stage stage;
    Texture background;

    float dt = 0;
    float timecounter = 0;

    //private HudInici hud;


    SpriteBatch sb;
    public ScreenTransition2(Principal game) {
        super(game);
        sb = new SpriteBatch();
        background = new Texture("pantallaInici/stage2.png");

        stage = new Stage();
        stage.setViewport(new ExtendViewport(640,480));

    }


    @Override
    public void show() {
        super.show();
    }


    @Override
    public void hide() {
        super.hide();
    }



    @Override
    public void render(float delta) {
        super.render(delta);
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        sb.begin();
        sb.draw(background, 0 ,0);

        sb.end();
        dt = Gdx.graphics.getDeltaTime();
        timecounter += dt;
        if(timecounter >= 4){
            timecounter = 0;
            game.musicTrack = 1;
            game.stopMusic();
            game.music();
            game.nivellActual=2;
            game.changeLevel();

        }


    }



    @Override
    public void dispose () {
        background.dispose();

        sb.dispose();


    }

}


