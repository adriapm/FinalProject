package main.principal.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.PolygonMapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

import java.io.FileWriter;
import java.io.PrintWriter;

import box2dLight.PointLight;
import box2dLight.RayHandler;
import main.principal.Actors.Coin;
import main.principal.Actors.Skills.Fireball;
import main.principal.Actors.GhostNpc;
import main.principal.Actors.Platform;
import main.principal.Actors.Pumpkin;
import main.principal.Actors.Wizzard;
import main.principal.Principal;
import main.principal.Tools.WorldContactListener;

/**
 * Created by adriapm on 07/05/18.
 */
//Tots els mapes hereden de BaseScreen
public class SegonMapa extends BaseScreen {
    //creem un Escenari ( stage ) on posarem tots els elements del mapa
    private Stage stage;

    private Pumpkin player;
    private GhostNpc ghost;
    private World world;

    //per fer les colisiones amb objectes del map;
    private Box2DDebugRenderer b2dr;
    private Wizzard wizzard;
    //private Viewport gamePort;
    //Carreguem el mapa creat en forma de blocs ( tiles )
    private TmxMapLoader maploader;
    private TiledMap map;
    private OrthogonalTiledMapRenderer renderer;
    float unitScale = 16;
    //    OrthographicCamera camera;
    private Coin coin;
    private Hud hud;
    RayHandler rayHandler;
    PointLight pointLight;
    PointLight pointLight2;

    public static Array<Coin> coins=new Array<Coin>();
    public static Array<Body> bCoins=new Array<Body>();

    public static Array<Fireball> fireballs=new Array<Fireball>();


    public SegonMapa(Principal game) {
        super(game);
        //Cargem el mapa
        //240*16
        //16*16

        //exemple per mi: 50×30 camb dimensio 16x16px per tile = 800×480

        map = new TmxMapLoader().load("maps/map2/map2.tmx");

        renderer = new OrthogonalTiledMapRenderer(map, 1 );

        hud =  game.hud;
        //Inicialitzem el mon i l'hi pasem al pumpkin

        world = new World(new Vector2(0,-10f), true);

        //Per fer que el mapa tingui objectes on el personatge interectui amb ells
        b2dr=new Box2DDebugRenderer();


        //per introduir una camara
        stage = new Stage();
        stage.setViewport(new ExtendViewport(640,480));
        stage.getCamera().position.set(10f, 10f, 1f);
        stage.getViewport().update(Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
        Pumpkin.setStage2(stage);
        Pumpkin.setWorld(world);
        Wizzard.setWorld(world);
        wizzard.setStage2(stage);
        GhostNpc.setWorld(world);
        GhostNpc.setStage2(stage);
        Wizzard.setWorld(world);
        Platform.setStage2(stage);
        Platform.setWorld(world);
        player = new Pumpkin(200,180);
        //ghost=new GhostNpc(player);


        //cridem el metode per afegir tots els actors al Escenari

        mapBodyBuilder();
        afegirActorsAlStage();


        rayHandler = new RayHandler(world);
        rayHandler.setShadows(true);
        rayHandler.useDiffuseLight(true);

        pointLight = new PointLight(rayHandler,100, Color.WHITE, 200, player.getX() , player.getY());

        pointLight2 = new PointLight(rayHandler,100, Color.WHITE, 200, player.getX() , player.getY());


        for (MapObject object : map.getLayers().get("Torch").getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();
            new PointLight(rayHandler,100, Color.ORANGE, 120, rect.getX() , rect.getY()).setXray(true);
        }

        world.setContactListener(new WorldContactListener(this));


        Hud.setLevel(2);

    }


    public void destroyFireball(Fireball fb) {
        fireballs.add(fb);
        fb.setVisible(false);
    }

    public void destroyCoin(String coin, Body bd){
        int x=Integer.valueOf(coin.substring(4));
        coins.get(x).dispose();
        coins.get(x).setVisible(false);
        bCoins.add(bd);
    }


    public void mapBodyBuilder(){

        BodyDef bdef = new BodyDef();
        PolygonShape shape = new PolygonShape();
        FixtureDef fdef = new FixtureDef();
        Body body;

        for (MapObject object : map.getLayers().get("GroundObject").getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();
            bdef.type = BodyDef.BodyType.StaticBody;
            bdef.position.set(rect.getX() + rect.getWidth() / 2, rect.getY() + rect.getHeight() / 2);
            body = world.createBody(bdef);
            shape.setAsBox(rect.getWidth() / 2, rect.getHeight() / 2);
            fdef.shape = shape;
            body.createFixture(fdef);
        }
        for (MapObject object : map.getLayers().get("LayerOfWizzard").getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();
            bdef.type = BodyDef.BodyType.StaticBody;
            bdef.position.set(rect.getX() + rect.getWidth() / 2, rect.getY() + rect.getHeight() / 2);
            body = world.createBody(bdef);
            shape.setAsBox(rect.getWidth() / 2, rect.getHeight() / 2);
            fdef.shape = shape;
            body.createFixture(fdef);
            Wizzard x = new Wizzard(player, (rect.getX() + rect.getWidth() / 2) * 2, (rect.getY() + rect.getHeight() / 2) * 2 + 65);
            stage.addActor(x);
        }
        int nCoin=0;
        for (MapObject object : map.getLayers().get("Coin").getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();
            bdef.type = BodyDef.BodyType.StaticBody;
            bdef.position.set(rect.getX() + rect.getWidth(), rect.getY() + rect.getHeight() + 4);
            body = world.createBody(bdef);
            shape.setAsBox(rect.getWidth() / 2, (rect.getHeight() / 2));
            fdef.shape = shape;
            body.createFixture(fdef).setUserData("coin"+nCoin);
            nCoin++;
            Coin x = new Coin((rect.getX() + rect.getWidth() / 2) * 2, (rect.getY() + rect.getHeight() / 2) * 2);
            coins.add(x);
            stage.addActor(x);
        }
        for (MapObject object : map.getLayers().get("Spikes").getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();
            bdef.type = BodyDef.BodyType.StaticBody;
            bdef.position.set(rect.getX() + rect.getWidth() / 2, rect.getY() + rect.getHeight() / 2);
            body = world.createBody(bdef);
            shape.setAsBox(rect.getWidth() / 2, rect.getHeight() / 2);
            fdef.shape = shape;
            body.createFixture(fdef).setUserData("spike");

        }
        for (MapObject object : map.getLayers().get("Ceiling").getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();
            bdef.type = BodyDef.BodyType.StaticBody;
            bdef.position.set(rect.getX() + rect.getWidth() / 2, rect.getY() + rect.getHeight() / 2);
            body = world.createBody(bdef);
            shape.setAsBox(rect.getWidth() / 2, rect.getHeight() / 2);
            fdef.shape = shape;
            body.createFixture(fdef).setUserData("Ceiling");
        }

       for (MapObject object : map.getLayers().get("Platform").getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();
           // bdef.type = BodyDef.BodyType.StaticBody;

          //  bdef.position.set(rect.getX() + rect.getWidth() / 2, rect.getY() + rect.getHeight() / 2);
           // body = world.createBody(bdef);
           // shape.setAsBox(rect.getWidth() / 2, rect.getHeight() / 2);
           // fdef.shape = shape;
           // body.createFixture(fdef).setUserData("platform");
           Platform platform = new Platform((rect.getX() + rect.getWidth() / 2) * 2, (rect.getY() + rect.getHeight() / 2) * 2, 1);
           stage.addActor(platform);
        }
        shape.dispose();

    }


    @Override
    public void show() {
        super.show();
    }


    @Override
    public void hide() {
        super.hide();
    }

    private static PolygonShape getPolygon(PolygonMapObject polygonObject) {
        PolygonShape polygon = new PolygonShape();
        float[] vertices = polygonObject.getPolygon().getTransformedVertices();

        float[] worldVertices = new float[vertices.length];

        for (int i = 0; i < vertices.length; ++i) {
            System.out.println(vertices[i]);
            worldVertices[i] = vertices[i] / 16;
        }

        polygon.set(worldVertices);
        return polygon;
    }

    public void gameInit(){
        player.init(200,180);
    }

    //El codi de render s'executa 60 cops per segon
    @Override
    public void render(float delta) {

        super.render(delta);

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        //Stage crida el metode draw de cada actor
        //Stage crida el metode act de cada actor
        stage.act();

        //perque la camara estigui amb el player

        world.step(1/60f, 6, 2);

        renderer.setView((OrthographicCamera)stage.getCamera());
      //  renderer.getBatch().setColor(0.1f, 0.1f, 0.1f,1f);
        renderer.render();

        //per renderitzar el nostre Box2DDebugLines (colisiones amb objectes del mapa)

       //  b2dr.render(world, stage.getCamera().combined);

        if (Pumpkin.getCurrentState().equals(Pumpkin.State.DEAD)) {
            game.setScreen(game.deadScreen);
            Hud.sumDeathCount();
            Hud.restartTime();
            game.stopMusic();
            game.deathSound();
        }

        stage.draw();
        pointLight2.setPosition(player.getX(), player.getY());
        pointLight2.setXray(false);
        pointLight.setPosition(player.getX(), player.getY());
        pointLight.setXray(true);
        rayHandler.setCombinedMatrix((OrthographicCamera) stage.getCamera());
        rayHandler.updateAndRender();


        game.batch.setProjectionMatrix(hud.stage.getCamera().combined);
        hud.stage.draw();

        hud.update();

        for (Body c : bCoins) {
            world.destroyBody(c);
        }
        bCoins.clear();

        for (Fireball c : fireballs) {
            world.destroyBody(c.getBody());
        }
        fireballs.clear();
        completedLevel();

    }

    //Afegim actors al escenari
    private void afegirActorsAlStage() {
       // stage.addActor(ghost);
        stage.addActor(player);
    }
    public void completedLevel(){

        if(player.getY()==1495.015f && player.getX()>1546.564f){
            FileWriter file = null;
            PrintWriter pw = null;
            try {
                file = new FileWriter("level.txt");
                pw = new PrintWriter(file);
                pw.println(2);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (null != file)
                        file.close();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            game.stopMusic();
            game.musicTrack = 3;
            game.music();
            Hud.endLevelScore();
            game.setScreen(game.endStage);
        }
    }
    //Lliberem recursos
    @Override
    public void dispose () {
        stage.clear();
        player.dispose();
        world.dispose();
        wizzard.dispose();
        ghost.dispose();
        renderer.dispose();
        map.dispose();
    }

}
