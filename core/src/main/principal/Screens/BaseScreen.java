package main.principal.Screens;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.physics.box2d.Body;

import main.principal.Actors.Skills.Fireball;
import main.principal.Principal;

/**
 * Created by adriapm on 07/05/18.
 */

public class BaseScreen implements Screen {
    protected Principal game;


    public BaseScreen(Principal game){

        this.game = game;
    }
    @Override
    public void show() {

    }


    @Override
    public void render(float delta) {

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }


    public void destroyFireball(Fireball fb) {
    }

    public void destroyCoin(String coin, Body bd){
    }
}
