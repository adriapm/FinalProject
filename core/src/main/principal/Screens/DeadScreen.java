package main.principal.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

import main.principal.Actors.Pumpkin;
import main.principal.Principal;

public class DeadScreen extends BaseScreen{


    public Stage stage;
    Texture background;
    Texture startString;
    SpriteBatch sb;
    float dt = 0;
    float timecounter = 0;

    public DeadScreen(Principal game) {
        super(game);

        sb = new SpriteBatch();
        background = new Texture("pantallaInici/mort.png");
        startString = new Texture("pantallaInici/titolMort.png");
        stage = new Stage();
        stage.setViewport(new ExtendViewport(640,480));
    }


    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        sb.begin();
        sb.draw(background, 0 ,0);

        dt = Gdx.graphics.getDeltaTime();
        timecounter += dt;
        if(timecounter >= 1){
            sb.draw(startString, 0 ,0);
            timecounter = 0;
        }
        sb.end();
        if(Gdx.input.isKeyJustPressed(Input.Keys.ANY_KEY)){
            if(game.nivellActual == 1){
                game.mapa1.gameInit();
                game.music();
                game.setScreen(game.mapa1);
            }else if(game.nivellActual == 2){
                game.mapa2.gameInit();
                game.music();
                game.setScreen(game.mapa2);
            }

        }





    }

    @Override
    public void dispose() {
        stage.dispose();
        background.dispose();
        startString.dispose();
        sb.dispose();
    }



}
