package main.principal.Screens;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

import main.principal.Principal;

/**
 * Created by adriapm on 18/05/18.
 */



public class IntroScreen extends BaseScreen {

    public Stage stage;
    Texture background;
    Texture startString;


    //private HudInici hud;


    SpriteBatch sb;
    public IntroScreen(Principal game) {
       super(game);
       sb = new SpriteBatch();
       background = new Texture("pantallaInici/fons.png");
       startString = new Texture("pantallaInici/titol.png");
       stage = new Stage();
       stage.setViewport(new ExtendViewport(640,480));

    }


    @Override
    public void show() {
        super.show();
    }


    @Override
    public void hide() {
        super.hide();
    }



    @Override
    public void render(float delta) {
        super.render(delta);
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        sb.begin();
        sb.draw(background, 0 ,0);
        sb.draw(startString, 0 ,0);
        sb.end();
        if(Gdx.input.isKeyJustPressed(Input.Keys.ANY_KEY)){

            game.musicTrack = 1;
            game.stopMusic();
            game.music();
            game.setScreen(game.mapa1);
        }

    }



    @Override
    public void dispose () {
        background.dispose();
        startString.dispose();
        sb.dispose();


    }

}


