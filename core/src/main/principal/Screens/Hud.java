package main.principal.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import main.principal.Actors.Pumpkin;

/**
 * Created by adriapm on 17/05/18.
 */

public class Hud implements Disposable {

    public Stage stage;
    public Viewport viewport;

    private float timecounter  = 0;
    public static int score;
    public static int level;
    public static int time;
    private static int deathCount;
    public String controls= "A | D | SPACE ";
    float dt;
    static Label scoreLabel;
    static Label levelLabel;
    Label levelStringLabel;
    Label timeLabel;
    Label timeStringLabel;
    static Label deathLabel;
    Label deathStringLabel;
    Label controlsLabel;
    Label controlsStringLabel;
    Label scoreStringLabel;

    public Hud(SpriteBatch sb){



        score = 0;
        level = 1;
        time = 300;
        deathCount = 0;

        viewport = new FitViewport(640,480, new OrthographicCamera());
        stage = new Stage(viewport, sb);

        Table table = new Table();

        table.setFillParent(true);

        scoreLabel =new Label(String.format("%06d", score), new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        scoreStringLabel =new Label("SCORE", new Label.LabelStyle(new BitmapFont(), Color.WHITE));

        timeLabel = new Label(String.format("%03d", time), new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        timeStringLabel = new Label("TIME", new Label.LabelStyle(new BitmapFont(), Color.WHITE));

        deathLabel = new Label(String.format("%d", deathCount), new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        deathStringLabel = new Label("DEATH", new Label.LabelStyle(new BitmapFont(), Color.WHITE));

        controlsLabel = new Label(controls, new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        controlsStringLabel = new Label("CONTROLS", new Label.LabelStyle(new BitmapFont(), Color.WHITE));

        levelLabel = new Label(String.format("%03d", level), new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        levelStringLabel = new Label("LEVEL", new Label.LabelStyle(new BitmapFont(), Color.WHITE));

        table.top();
        table.add(levelStringLabel).expandX().pad(10);
        table.add(deathStringLabel).expandX().pad(10);
        table.add(timeStringLabel).expandX().pad(10);
        table.add(controlsStringLabel).expandX().pad(10);
        table.add(scoreStringLabel).expandX().pad(10);
        table.row();
        table.add(levelLabel).expandX();
        table.add(deathLabel).expandX();
        table.add(timeLabel).expandX();
        table.add(controlsLabel).expandX();
        table.add(scoreLabel).expandX().pad(10);


        stage.addActor(table);
    }

    public void update(){
        dt = Gdx.graphics.getDeltaTime();
        timecounter += dt;
        if(timecounter >= 1){
            time--;
            timeLabel.setText(String.format("%03d", time));
            if(time > 0){

                timecounter = 0;
            }else {
               Pumpkin.setCurrentState(Pumpkin.State.DEAD);
            }
        }



    }

    public static void restartTime() {
        time = 300;
    }

    public static void sumDeathCount(){
        deathCount++;
        deathLabel.setText(String.format("%d", deathCount));
    }


    public static void sumScore(int num){
        score += num;
        scoreLabel.setText(String.format("%06d", score));
    }
    public static void subtractScore(int num){
        if(score - num <= 0){
            score = 0;
        }else{
            score -= num;
        }

        scoreLabel.setText(String.format("%06d", score));
    }



    @Override
    public void dispose() {
        stage.dispose();

    }

    public static void setLevel(int i) {
        level = i;
        levelLabel.setText(String.format("%03d", level));
    }



    public static void endLevelScore() {
        score = ((score * (time/4))/2);
        scoreLabel.setText(String.format("%06d", score));
    }
}
