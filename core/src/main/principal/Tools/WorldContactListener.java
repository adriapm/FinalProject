package main.principal.Tools;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;

import main.principal.Actors.Skills.Fireball;
import main.principal.Actors.Platform;
import main.principal.Actors.Pumpkin;
import main.principal.Screens.BaseScreen;
import main.principal.Screens.Hud;

public class WorldContactListener implements ContactListener {

    private BaseScreen map;  // JODER
    public WorldContactListener(BaseScreen map) {
        this.map = map;
    }
    Sound coinSound;

    @Override
    public void beginContact(Contact contact) {
        Fixture fixA = contact.getFixtureA();
        Fixture fixB = contact.getFixtureB();
       // Gdx.app.log("saltnat","s"  +fixA.getUserData()+ "   e   "+fixB.getUserData());
        if(fixA.getUserData()!=null && fixB.getUserData()!=null){
            if(fixA.getUserData() instanceof Pumpkin && fixB.getUserData() instanceof Fireball){
                Pumpkin.setCurrentState(Pumpkin.State.DEAD);
                ((Fireball) fixB.getUserData()).setDestroy(true);
            }
            if(fixA.getUserData() instanceof Pumpkin && fixB.getUserData().toString().contains("coin")){
                map.destroyCoin(fixB.getUserData().toString(), fixB.getBody());
                coinSound =  Gdx.audio.newSound(Gdx.files.internal("coin/coin1.wav"));
                coinSound.play(1.0f);
                Hud.sumScore(200);
            }
            if (fixA.getUserData() instanceof Pumpkin && fixB.getUserData().equals("spike")){
                Pumpkin.setCurrentState(Pumpkin.State.DEAD);
            }
            if(fixA.getUserData() instanceof Pumpkin && fixB.getUserData() instanceof Platform){
                ((Platform) fixB.getUserData()).platformBody.setGravityScale(-100);
                ((Pumpkin) fixA.getUserData()).getBody().setLinearVelocity( ((Platform) fixB.getUserData()).getBody().getLinearVelocity().x, ((Platform) fixB.getUserData()).getBody().getLinearVelocity().y);
            }
            if(fixA.getUserData() instanceof Pumpkin && fixB.getUserData().equals("Ceiling")){
                Pumpkin.setCurrentState(Pumpkin.State.CEILING);
            }
        }

        if(fixA.getUserData()==null && fixB.getUserData()!=null){
            if(fixB.getUserData() instanceof Fireball){
                System.out.println("Contacte");
                ((Fireball) fixB.getUserData()).setDestroy(true);


            }
        }

    }

    @Override
    public void endContact(Contact contact) {
        Fixture fixA = contact.getFixtureA();
        Fixture fixB = contact.getFixtureB();
        if(fixA.getUserData() instanceof Pumpkin && fixB.getUserData() instanceof Platform){
            ((Platform) fixB.getUserData()).platformBody.setGravityScale(0);
         
        }
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {
    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {
    }

}
