package main.principal;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import main.principal.Screens.DeadScreen;
import main.principal.Screens.EndScreen;
import main.principal.Screens.Hud;
import main.principal.Screens.IntroScreen;
import main.principal.Screens.PrimerMapa;
import main.principal.Screens.Proves;
import main.principal.Screens.ScreenTransition2;
import main.principal.Screens.SegonMapa;

public class Principal extends Game{
//Aqui es definiran els mapes que tindra el joc
	public PrimerMapa mapa1;
	public SegonMapa mapa2;
	public Proves x;
	public ScreenTransition2 stage2;
	public DeadScreen deadScreen;
	public SpriteBatch batch;
	public IntroScreen introScreen;
	public EndScreen endStage;
	public int nivellActual = checkLevel();
	public Hud hud;
	Sound sound;
	Sound sound2;
	Sound winSound;

	public int musicTrack = 0;

	//Box2D Collision Bits
	public static final short coin_bit = 7;

	@Override
	public void create () {
		//inicialitzem els mapes i cridem el primer

		batch = new SpriteBatch();
		hud = new Hud(batch);
		introScreen = new IntroScreen(this);
		deadScreen=new DeadScreen(this);
		stage2 = new ScreenTransition2(this);
		endStage = new EndScreen(this);
		changeLevel();
		music();
	}

	public void music() {
		//stopMusic();

		if(musicTrack == 0){
			sound = Gdx.audio.newSound(Gdx.files.internal("music/main.wav"));
			sound.play(0.6f);

		}else if(musicTrack == 1){
			sound = Gdx.audio.newSound(Gdx.files.internal("music/mapa.wav"));
			sound.play(0.6f);
		}else {
			sound = Gdx.audio.newSound(Gdx.files.internal("music/endSong.wav"));
			sound.play(0.6f);
		}
		sound.loop();

	}
	public void stopMusic(){
		sound.stop();
	}
	public void deathSound(){

		sound2  = Gdx.audio.newSound(Gdx.files.internal("music/deathSound.wav"));
		sound2.play(0.6f);

	}
	public void winSound(){
		winSound = Gdx.audio.newSound(Gdx.files.internal("music/winSong.wav"));
		winSound.play(0.6f);
	}

	@Override
	public void dispose () {
		mapa1.dispose();
		mapa2.dispose();
		deadScreen.dispose();
		introScreen.dispose();
		sound.dispose();
		sound2.dispose();
		winSound.dispose();
		batch.dispose();
		hud.dispose();
	}

	public int checkLevel(){

			File file = null;
			FileReader fr = null;
			BufferedReader br = null;
			int x = 1;
			try {
				file = new File("level.txt");
				fr = new FileReader(file);
				br = new BufferedReader(fr);

				String line;
				while ((line = br.readLine()) != null) {
					x = Integer.valueOf(line);
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (null != fr) {
						fr.close();
						br.close();
					}
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}
			return x;
		}

	public void changeLevel() {
		if(nivellActual==1) {
			mapa1 = new PrimerMapa(this);

			setScreen(introScreen);
		}else if (nivellActual==2){
			mapa2 = new SegonMapa(this);

			setScreen(mapa2);
		}
	}
}
