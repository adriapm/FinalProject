package main.principal.Actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class Platform extends Actor {

    //Textura i mides
    Texture texture;
    private Sprite platformSprite;
    private final int WIDTH = 48;
    private final int HEIGHT = 16;

    //Body
    public Body platformBody;
    private Fixture platformFixture;

    //World i stage
    static World world;
    static Stage stage;


    private int direction;




    public Platform(float xOwner, float yOwner, int direction){

        this.direction=direction;
        texture = new Texture("platform/platform.png");//Carreguem la imatge en forma de textura
        platformSprite = new Sprite(texture);//Creem un sprite i afegim la textura creada anteriorment
        platformSprite.setSize(WIDTH, HEIGHT);//Definim l'altura i amplada del nostre sprite
        platformSprite.setRegion(0, HEIGHT, WIDTH,HEIGHT);//El set region ens mostra el tros de textura que volem mostrar per donar un efecte animat
        platformSprite.flip(true, false);//Efecte mirall del sprite aixi optimitza la quantitat de dades a carregar
        setPosition(xOwner, yOwner);
        platformBody =  world.createBody(crearBodyDef(new Vector2(getX()/2,getY()/2)));

        crearPoligonFixture();

        setSize(WIDTH, HEIGHT);
        Vector2 pos = platformBody.getPosition();
        platformSprite.setCenter(pos.x, pos.y);
        //per fer colisions
        platformFixture.setUserData(this);


    }


    public Body getBody(){
        return platformBody;
    }

    @Override
    public void draw(Batch batch, float alpha){
        //Pintem el personatge en el joc
        setPosition(getBody().getPosition().x-4, getBody().getPosition().y);


        batch.draw(platformSprite, getX()-WIDTH/3 , getY()-HEIGHT/2,getWidth(),getHeight());
    }


    @Override
    public void act(float delta){
        checkDirection();
        if (direction==1) {
            platformBody.setLinearVelocity(new Vector2(+1000, 0));
        }else{
            platformBody.setLinearVelocity(new Vector2(-1000, 0));
        }
    }

    private void checkDirection() {
        if(platformBody.getLinearVelocity().x == 0){
            if(direction == 1){
                direction = 0;
            }else {
                direction = 1;
            }
        }
    }


    //Agafem el world per poder fer els bodys, calcular colisions etc.
    public static void setWorld(World wd){
        world = wd;
    }

    //Agafem el stage per poder moure la camara
    public static void setStage2(Stage st){
        stage = st;
    }

    public BodyDef crearBodyDef(Vector2 posicio) {
        BodyDef bd = new BodyDef();
        bd.position.set(posicio); // posicio on estara al poligon
        bd.type = BodyDef.BodyType.DynamicBody;
        return bd;
    }


    public void crearPoligonFixture() {

        PolygonShape poligon;//Creem el poligon
        poligon = new PolygonShape(); // tipus de poligon
        poligon.setAsBox((WIDTH)/2, (HEIGHT)/2); // mida del poligon

        FixtureDef fdef = new FixtureDef();//Creem la fixtureDef
        fdef.shape = poligon;//Asignem el poligon
        platformFixture = platformBody.createFixture(fdef);//creem la fixture a partir del body
        platformFixture.setUserData(this);
        poligon.dispose();//destruim el poligon
    }


    //Lliberem recursos
    public void dispose () {
        texture.dispose();
        world.destroyBody(platformBody);

    }

}
