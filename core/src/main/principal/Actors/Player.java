package main.principal.Actors;

/**
 * Created by adriapm on 15/05/18.
 */


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;




public class Player extends Actor {


    private Texture imatge;

    private static World world;
    private static Stage stage;

    private Body body;

    private Fixture fixture;

    private BodyDef bodyDef;
    private PolygonShape poligon;
    private int X, Y;
    private boolean cambiarPantallaCombat, cambiarPantallaEstat;

    private  Texture [] animacioDreta = new Texture[10];
    private int animacioDretaContador;
    private boolean activarAnimacioDreta;



    public static void setWorld(World w) {
        world = w;

    }

    public static void setStage2(Stage s) {
        stage = s;
    }


    public Player(Vector2 posicio) {

        imatge = new Texture("badlogic.jpg");

        bodyDef = crearBodyDef(posicio);
        crearPoligonFixture();
        setSize(50, 50);
        cambiarPantallaCombat = false;
        cambiarPantallaEstat = false;
        afegirListenerOnEstaPersonatge();


        animacioDretaContador=0;
        activarAnimacioDreta=false;
    }


    public Texture getImatge (){
        return imatge;
    }

    public void afegirListenerOnEstaPersonatge() {

        //this.setBounds(getX(), getY(), getWidth(), getHeight());

        this.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                //System.out.println("Ale hop");
                //System.out.println(Mides.potColisionar);
                cambiarPantallaEstat = true;
                return false;
            }


            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                //System.out.println("up22");
            }
        });
    }

    public boolean getCambiarPantallaEstat (){
        return cambiarPantallaEstat;
    }
    public void setCambiarPantallaEstat(boolean b){
        cambiarPantallaEstat = b;
    }

    public boolean getCambiarPantallaCombat (){
        return cambiarPantallaCombat;
    }
    public void setCambiarPantallaCombat(boolean b){
        cambiarPantallaEstat = b;
    }


    @Override
    public void draw(Batch batch, float parentAlpha) {
        // al posar la posicio restem 0.5 perque quede be, ja que 0.5 es la mida del poligon
        setPosition((body.getPosition().x - 0.5f) * 50,
                (body.getPosition().y - 0.5f) * 50);

        batch.draw(imatge, getX(), getY(), getWidth(), getHeight());
    }

    public Body getBody(){
        return  body;
    }


    public BodyDef crearBodyDef(Vector2 posicio) {
        BodyDef bd = new BodyDef();
        bd.position.set(posicio); // posicio on estara al poligon
        bd.type = BodyDef.BodyType.DynamicBody;
        body = world.createBody(bd);
        return bd;
    }


    public void crearPoligonFixture() {
        poligon = new PolygonShape(); // tipus de poligon
        poligon.setAsBox(0.5f, 0.5f);  // mida del poligon
        fixture = body.createFixture(poligon, 0);  // el numero es la densitat
        fixture.setDensity(0);
        fixture.setUserData("jugador");
        /*fixture.setFriction(0);
        fixture.setRestitution(0);
        fixture.setSensor(true);*/
        poligon.dispose();
    }

    public void lliberarRecursos() {
        if (fixture != null) {
            body.destroyFixture(fixture);
        }
        world.destroyBody(body);
        imatge.dispose();
    }
}
