package main.principal.Actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;

/**
 * Created by adriapm on 07/05/18.
 */

public class GhostNpc extends Actor {
    Texture texture;
    static World world;
    static Stage stage;
    private Body ghostBody;
    private BodyDef ghostBodyDef;
    private Sprite ghostSprite;
    private final int WIDTH = 33;//Amplada del personatge
    private final int HEIGHT = 78;//Altura del personatge
    private float ghostX;
    private float ghostY;
    private float ghostSpeed;
    private float ghostSpeedY;
    private int pas = 0;//El tros de textura que volem que es mostri
    private Pumpkin target;
    private int meter;

    //Constructor de ghost
    public GhostNpc(Pumpkin target){
        this.target=target;
        ghostX = 0;
        ghostY = 0;
        ghostSpeed = target.pumpkinSpeed/2;
        ghostSpeedY= target.pumpkinSpeed-150/2;
        texture = new Texture("npcs/ghost.png");//Carreguem la imatge en forma de textura
        ghostSprite = new Sprite(texture);//Creem un sprite i afegim la textura creada anteriorment
        ghostSprite.setSize(WIDTH, HEIGHT);//Definim l'altura i amplada del nostre sprite
        ghostSprite.setRegion(0, HEIGHT , WIDTH, HEIGHT);
        //El set region ens mostra el tros de textura que volem mostrar per donar un efecte animat

        ghostSprite.flip(true, false);//Efecte mirall del sprite aixi optimitza la quantitat de dades a carregar

        ghostBodyDef = createGhostBodyDef();//Per crear un body necessitem un bodyDef y un world, aixi que cridem el metode de crear bodyDef i el retornem
        ghostBody = world.createBody(ghostBodyDef);//L'hi diem al world que ens creei un body a partir d'un bodyDef i aixi ens retorna el body.
    }

    //Creem el bodyDef y el retornem
    private BodyDef createGhostBodyDef() {
        BodyDef bd = new BodyDef();
        bd.position.set(0, 10);
        bd.type = BodyDef.BodyType.DynamicBody;//Tipus de body que volem, en aquest cas com es moura per el mapa és dynamic body
        return bd;
    }


    public float getGhostX() {
        return ghostX;
    }

    public void setGhostX(float ghostX) {
        this.ghostX = ghostX;
    }

    public float getGhostY() {
        return ghostY;
    }

    public void setGhostY(float ghostY) {
        this.ghostY = ghostY;
    }

    public static void setStage2(Stage st){
        stage=st;
    }

    @Override
    public void draw(Batch batch, float alpha) {
        //Pintem el personatge en el joc
        ghostSprite.setRegion(0, HEIGHT * (pas % 10), WIDTH, HEIGHT);
        if (Math.round(ghostX)-Math.round(target.getX())<-10) {
            ghostSprite.flip(true, false);
        }
        batch.draw(ghostSprite, ghostX, ghostY);
    }

    @Override
    public void act(float delta) {
        meter++;

        //per fer la animacio
        if (meter % 2 == 0) {
            pas++;
        }
        if(pas>2){
            pas=0;
        }
        if(ghostX>target.getX()){
            ghostX -= Gdx.graphics.getDeltaTime() * ghostSpeed;
        }else if(ghostX<target.getX()){
            ghostX += Gdx.graphics.getDeltaTime() * ghostSpeed;
        }

        if(ghostY>target.getY()){
            ghostY -= Gdx.graphics.getDeltaTime()*ghostSpeedY;
        }else if(ghostY<target.getY()){
            ghostY += Gdx.graphics.getDeltaTime() * ghostSpeedY;
        }

        if(Math.round(ghostX)-Math.round(target.getX())>-30 && Math.round(ghostX)-Math.round(target.getX())<30){
            if(Math.round(ghostY)-Math.round(target.getY())>-10 && Math.round(ghostY)-Math.round(target.getY())<10){
                target.setCurrentState(Pumpkin.State.DEAD);
            }
        }


    }

    //Agafem el world per poder fer els bodys, calcular colisions etc.
    public static void setWorld(World wd) {
        world = wd;
    }

    //Lliberem recursos
    public void dispose() {
        texture.dispose();
        world.destroyBody(ghostBody);
        world.dispose();
    }
}