package main.principal.Actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;

/**
 * Created by adriapm on 07/05/18.
 */

public class Coin extends Actor {
    Texture texture;
    static World world;
    static Stage stage;
    final int  WIDTH = 25;
    final int HEIGHT = 25;
    private Sprite coin;
    private int step = 0;
    private int meter = 0;
    private Body coinBody;
    private Fixture coinFixture;

    Sound coinSound;
    private int pas = 0;//El tros de textura que volem que es mostri
    private Pumpkin target;


    //Constructor de ghost
    public Coin(float x, float y){

       texture = new Texture("coin/coin.png");
       coin = new Sprite(texture);
       coin.setSize(WIDTH, HEIGHT);
       setPosition(x,y);
       coin.setRegion(WIDTH*(step+1), 0, WIDTH,HEIGHT);

    }

    public static void setStage2(Stage st){
        stage=st;
    }

    @Override
    public void draw(Batch batch, float alpha) {
        //Pintem el personatge en el joc
        coin.setRegion(WIDTH*(step%10), 0, WIDTH,HEIGHT);
        batch.draw(coin,getX()/2,getY()/2);
    }

    @Override
    public void act(float delta) {
        meter++;
        if (meter % 4 == 0) {
            step++;
        }

    }

    //Agafem el world per poder fer els bodys, calcular colisions etc.
    public static void setWorld(World wd) {
        world = wd;
    }

    //Lliberem recursos
    public void dispose() {
        texture.dispose();

    }
}