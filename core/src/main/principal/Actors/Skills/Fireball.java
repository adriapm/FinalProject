package main.principal.Actors.Skills;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.JointEdge;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;

import java.util.ArrayList;

import main.principal.Actors.Pumpkin;

public class Fireball extends Actor {

    //Textura i mides
    Texture texture;
    private Sprite fireballSprite;
    private final int WIDTH = 16;//Amplada del personatge
    private final int HEIGHT = 16;//Altura del personatge

    //Body
    private Body fireballBody;
    private Fixture fireballFixture;
    private boolean destroy = false;

    //World i stage
    static World world;
    static Stage stage;

    private int step = 0;
    private int direction;
    //target
    private Pumpkin target;
    private float xOwner;
    private float yOwner;

    public Fireball(Pumpkin target, float xOwner, float yOwner, int direction){
        this.xOwner=xOwner;
        this.yOwner=yOwner;

        this.target=target;
        this.direction=direction;
        texture = new Texture("npcs/fireball.png");//Carreguem la imatge en forma de textura
        fireballSprite = new Sprite(texture);//Creem un sprite i afegim la textura creada anteriorment
        fireballSprite.setSize(WIDTH, HEIGHT);//Definim l'altura i amplada del nostre sprite
        fireballSprite.setRegion(0, HEIGHT, WIDTH,HEIGHT);//El set region ens mostra el tros de textura que volem mostrar per donar un efecte animat
        fireballSprite.flip(true, false);//Efecte mirall del sprite aixi optimitza la quantitat de dades a carregar
        if(direction==0){
            setPosition(xOwner*2-40,yOwner*2);
        }else{
            setPosition(xOwner*2+40,yOwner*2);
        }
        fireballBody =  world.createBody(crearBodyDef(new Vector2(getX()/2,getY()/2)));

        crearPoligonFixture();

        setSize(WIDTH, HEIGHT);
        Vector2 pos = fireballBody.getPosition();
        fireballSprite.setCenter(pos.x, pos.y);
        //per fer colisions
        fireballFixture.setUserData(this);
    }


    public Body getBody(){
        return fireballBody;
    }

    @Override
    public void draw(Batch batch, float alpha){
        //Pintem el personatge en el joc
        setPosition(getBody().getPosition().x-4, getBody().getPosition().y);
        fireballSprite.setRegion(0, HEIGHT*(step%10), WIDTH,HEIGHT);
        if (Math.round(getX())-Math.round(target.getX())<-10) {
            fireballSprite.flip(true, false);
        }
        batch.draw(fireballSprite, getX()-WIDTH/3 , getY()-HEIGHT/2,getWidth(),getHeight());
    }


    @Override
    public void act(float delta){

        if (direction==1) {
            fireballBody.setLinearVelocity(new Vector2(+500, 0));
        }else{
            fireballBody.setLinearVelocity(new Vector2(-500, 0));
        }
    }


    //Agafem el world per poder fer els bodys, calcular colisions etc.
    public static void setWorld(World wd){
        world = wd;
    }

    //Agafem el stage per poder moure la camara
    public static void setStage2(Stage st){
        stage = st;
    }

    public BodyDef crearBodyDef(Vector2 posicio) {
        BodyDef bd = new BodyDef();
        bd.position.set(posicio); // posicio on estara al poligon
        bd.type = BodyDef.BodyType.DynamicBody;
        return bd;
    }

    public int getDirection(){
        return direction;
    }


    public void restart(int direction, int st){

        /*
        world.destroyBody(fireballBody);
        if(direction==0){
            System.out.println("reset 0");
            setPosition(xOwner*2-40,yOwner*2);
        }else{
            System.out.println("reset 1");
            setPosition(xOwner*2+40,yOwner*2);
        }

        crearPoligonFixture();
        setDestroy(false);
        setSize(WIDTH, HEIGHT);
        Vector2 pos = fireballBody.getPosition();
        fireballSprite.setCenter(pos.x, pos.y);
        //per fer colisions
        fireballFixture.setUserData(this);
        */
            if(st==3){
                world.destroyBody(fireballBody);
                setVisible(true);
                this.direction=direction;
                if(direction==0){
                    setPosition(xOwner*2-40,yOwner*2);
                    fireballBody = world.createBody(crearBodyDef(new Vector2((xOwner*2-40)/2,(yOwner*2)/2)));
                }else{
                    setPosition(xOwner*2+40,yOwner*2);
                    fireballBody = world.createBody(crearBodyDef(new Vector2((xOwner*2+40)/2,(yOwner*2)/2)));
                }
                crearPoligonFixture();
                setSize(WIDTH, HEIGHT);
                Vector2 pos = fireballBody.getPosition();
                fireballSprite.setCenter(pos.x, pos.y);
                fireballFixture.setUserData(this);
                setDestroy(false);
            }else{
                world.destroyBody(fireballBody);
                setVisible(false);
                fireballBody = world.createBody(crearBodyDef(new Vector2((xOwner*2+4000)/2,(yOwner*2+400)/2)));
            }
    }



    public void crearPoligonFixture() {

        PolygonShape poligon;//Creem el poligon
        poligon = new PolygonShape(); // tipus de poligon
        poligon.setAsBox((WIDTH)/2, (HEIGHT)/2); // mida del poligon

        FixtureDef fdef = new FixtureDef();//Creem la fixtureDef
        fdef.shape = poligon;//Asignem el poligon
        fireballFixture = fireballBody.createFixture(fdef);//creem la fixture a partir del body
        fireballFixture.setUserData(this);
        poligon.dispose();//destruim el poligon

    }

    public boolean getDestroy(){
        return destroy;
    }
    public void setDestroy(boolean bol){
        destroy = bol;
    }

    //Lliberem recursos
    public void dispose () {
        texture.dispose();
        world.destroyBody(fireballBody);
    }

}
