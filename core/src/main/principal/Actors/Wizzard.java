package main.principal.Actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;

import main.principal.Actors.Skills.Fireball;

public class Wizzard extends Actor {

    //Textura i mides
    Texture texture;
    private Sprite wizzardSprite;
    private final int WIDTH = 40;//Amplada del personatge
    private final int HEIGHT = 56;//Altura del personatge
    private Fireball fb;
    private int direction;
    //Body
    private Body wizzardBody;
    private Fixture wizzardFixture;

    //World i stage
    static World world;
    static Stage stage;

    private int step = 0;
    private int meter = 0;

    //target
    private Pumpkin target;

    private boolean fbFire=false;

    public Wizzard(Pumpkin target, float x, float y){
        this.target=target;
        loadSprite();

        setPosition(x,y);

        wizzardBody =  world.createBody(crearBodyDef(new Vector2(getX()/2,getY()/2)));

        crearPoligonFixture();

        setSize(WIDTH, HEIGHT);
        Vector2 pos = wizzardBody.getPosition();
        wizzardSprite.setCenter(pos.x, pos.y);
        fbLoad();
    }

    public void loadSprite() {
        texture = new Texture("npcs/wizzard1.png");//Carreguem la imatge en forma de textura
        wizzardSprite = new Sprite(texture);//Creem un sprite i afegim la textura creada anteriorment
        wizzardSprite.setSize(WIDTH, HEIGHT);//Definim l'altura i amplada del nostre sprite
        wizzardSprite.setRegion(0, HEIGHT*(step+1), WIDTH,HEIGHT);//El set region ens mostra el tros de textura que volem mostrar per donar un efecte animat
        wizzardSprite.flip(true, false);//Efecte mirall del sprite aixi optimitza la quantitat de dades a carregar
    }


    public Body getBody(){
        return wizzardBody;
    }

    @Override
    public void draw(Batch batch, float alpha){
        //Pintem el personatge en el joc
        setPosition(getBody().getPosition().x-8, getBody().getPosition().y);
        wizzardSprite.setRegion(0, HEIGHT*(step%10), WIDTH,HEIGHT);
        if (Math.round(getX())-Math.round(target.getX())<-10) {
            wizzardSprite.flip(true, false);
            direction=1;
        }else{
            direction=0;
        }
        batch.draw(wizzardSprite, getX()-WIDTH/3 , getY()-HEIGHT/2,getWidth(),getHeight());
    }


    @Override
    public void act(float delta){

        meter++;
        //per poder-lo esquivar
        if (meter % 100 == 2) {
            step++;
        }

        if(step>2){
            if(fbFire==false){
                fbLoad();
                fbFire=true;
            }
        }


        //abans de tirar un altre borrem la que hi ha

        if(step==3 || fb.getDestroy() == true ){
            fb.restart(direction,step);
        }

        if(step>2){
            step=0;
        }

    }

    public void fbLoad(){
        Fireball.setWorld(world);
        fb= new Fireball(target, getX(), getY(),direction);
        stage.addActor(fb);
    }


    //Agafem el world per poder fer els bodys, calcular colisions etc.
    public static void setWorld(World wd){
        world = wd;
    }

    //Agafem el stage per poder moure la camara
    public static void setStage2(Stage st){
        stage = st;
    }

    public BodyDef crearBodyDef(Vector2 posicio) {
        BodyDef bd = new BodyDef();
        bd.position.set(posicio); // posicio on estara al poligon
        bd.type = BodyDef.BodyType.StaticBody;
        return bd;
    }


    public void crearPoligonFixture() {

        PolygonShape poligon;//Creem el poligon
        poligon = new PolygonShape(); // tipus de poligon
        poligon.setAsBox((WIDTH+10)/3, (HEIGHT-10)/2);

        FixtureDef fdef = new FixtureDef();//Creem la fixtureDef
        fdef.shape = poligon;//Asignem el poligon
        wizzardFixture = wizzardBody.createFixture(fdef);//creem la fixture a partir del body
        wizzardFixture.setUserData(this);
        poligon.dispose();//destruim el poligon
    }


    //Lliberem recursos
    public void dispose () {
        texture.dispose();
        world.destroyBody(wizzardBody);
    }
}
