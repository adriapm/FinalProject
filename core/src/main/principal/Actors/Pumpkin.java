package main.principal.Actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;

/**
 * Created by adriapm on 07/05/18.
 */

public class Pumpkin extends Actor {

    //Estat
    public enum State {
        FALLING, JUMPING, STANDING, RUNNING, DEAD, CEILING
    }

    public static State currentState;

    //Textura i mides
    Texture texture;
    private Sprite pumpKinSprite;
    private final int WIDTH = 32;//Amplada del personatge
    private final int HEIGHT = 56;//Altura del personatge

    //Body
    private Body pumpkinBody;
    private Fixture pumpkinFixture;

    //World i stage
    static World world;
    static Stage stage;

    //Altres
    private float initPosX;
    private float initPosY;
    public float pumpkinSpeed;
    private int countJump = 0;
    private int step = 0;//El tros de textura que volem que es mostri
    private int direction = 1;
    private int meter = 0;
    private int doubleJump = 2;

    //Constructor de Pumpkin
    public Pumpkin(float initPosX, float initPosY) {
        this.initPosX = initPosX;
        this.initPosY = initPosY;
        pumpkinSpeed = 100;

        loadSprite();

        setPosition(initPosX, initPosY);

        pumpkinBody = world.createBody(crearBodyDef(new Vector2(getX() / 2, getY() / 2)));

        crearPoligonFixture();

        setSize(WIDTH, HEIGHT);
        Vector2 pos = pumpkinBody.getPosition();
        pumpKinSprite.setCenter(pos.x, pos.y);
        currentState = State.STANDING;
        pumpkinBody.setGravityScale(50);

    }

    public void loadSprite() {
        texture = new Texture("player/pumpkin.png");//Carreguem la imatge en forma de textura
        pumpKinSprite = new Sprite(texture);//Creem un sprite i afegim la textura creada anteriorment
        pumpKinSprite.setSize(WIDTH, HEIGHT);//Definim l'altura i amplada del nostre sprite
        pumpKinSprite.setRegion(0, HEIGHT * (step + 1), WIDTH, HEIGHT);//El set region ens mostra el tros de textura que volem mostrar per donar un efecte animat
        pumpKinSprite.flip(true, false);//Efecte mirall del sprite aixi optimitza la quantitat de dades a carregar
    }

    public int getDirection() {
        return direction;
    }

    public float getPumpkinX() {
        return getX();
    }

    public static State getCurrentState() {
        return currentState;
    }

    public static void setCurrentState(State currentState) {
        Pumpkin.currentState = currentState;
    }

    //Getter del body
    public Body getBody() {
        return pumpkinBody;
    }

    @Override
    public void draw(Batch batch, float alpha) {
        //Pintem el personatge en el joc
        setPosition(getBody().getPosition().x - 4, getBody().getPosition().y);

        pumpKinSprite.setRegion(0, HEIGHT * (step % 10), WIDTH, HEIGHT);
        if (direction == 1) {
            pumpKinSprite.flip(true, false);
        }

        batch.draw(pumpKinSprite, getX() - WIDTH / 3, getY() - HEIGHT / 2, getWidth(), getHeight());

    }


    @Override
    public void act(float delta) {
        //asignem un estat

        if (currentState != currentState.DEAD) {

            //per si toca el sostre
            if (Pumpkin.getCurrentState().equals(State.CEILING)) {
                pumpkinBody.applyLinearImpulse(new Vector2(pumpkinBody.getLinearVelocity().x, -500), pumpkinBody.getWorldCenter(), true);
                pumpkinBody.setGravityScale(50);
                countJump = 0;
            }

            currentState = getState();

            if (Gdx.input.isKeyPressed(Input.Keys.D)) {
                inputKeyD();
            } else if (Gdx.input.isKeyPressed(Input.Keys.A)) {
                inputKeyA();
            } else {
                if (currentState != State.JUMPING && currentState != State.FALLING) {
                    pumpkinBody.setLinearVelocity(new Vector2(0, 0));
                } else if (currentState == State.FALLING || currentState == State.RUNNING) {
                    pumpkinBody.setLinearVelocity(new Vector2(0, -500));
                }
            }

            if (pumpkinBody.getGravityScale() == -50 ) {
                countJump++;
                if (countJump > 40) {
                    pumpkinBody.applyLinearImpulse(new Vector2(pumpkinBody.getLinearVelocity().x, -500), pumpkinBody.getWorldCenter(), true);
                    countJump = 0;
                    pumpkinBody.setGravityScale(50);

                }
            }

            //Salt del Pumpkin
            if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE) && (( currentState != State.FALLING) || doubleJump > 0)) {
                inputSpaceKey();
            }

            doubleJump = checkDoubleJump(doubleJump);
        }
        //Si esta quiet es reinicia la animacio
        if (currentState == State.STANDING) {
            step = 0;
        }
        //Modifiquem la camara
        stage.getCamera().position.x = getX();
        stage.getCamera().position.y = getY();
    }



    public void inputSpaceKey() {

        if (doubleJump != 1) {
            pumpkinBody.setGravityScale(-50);
            pumpkinBody.applyLinearImpulse(new Vector2(pumpkinBody.getLinearVelocity().x, 500), pumpkinBody.getWorldCenter(), true);
            doubleJump--;
        } else {
            countJump=0;
            pumpkinBody.setGravityScale(-50);
            pumpkinBody.applyLinearImpulse(new Vector2(pumpkinBody.getLinearVelocity().x, 250), pumpkinBody.getWorldCenter(), true);
            doubleJump--;
        }
    }


    public void inputKeyA() {
        meter++;
        pumpkinBody.applyLinearImpulse(new Vector2(-6, 0), pumpkinBody.getWorldCenter(), true);
        direction = 0;
        if (meter % 2 == 0) {
            step++;
        }
    }

    public void inputKeyD() {
        meter++;
        pumpkinBody.applyLinearImpulse(new Vector2(6, 0), pumpkinBody.getWorldCenter(), true);
        direction = 1;
        if (meter % 2 == 0) {
            step++;
        }
    }


    //Agafem el world per poder fer els bodys, calcular colisions etc.
    public static void setWorld(World wd) {
        world = wd;
    }

    //Agafem el stage per poder moure la camara
    public static void setStage2(Stage st) {
        stage = st;
    }

    public BodyDef crearBodyDef(Vector2 posicio) {
        BodyDef bd = new BodyDef();
        bd.position.set(posicio); // posicio on estara al poligon
        bd.type = BodyDef.BodyType.DynamicBody;
        return bd;
    }


    public void crearPoligonFixture() {
        PolygonShape poligon;//Creem el poligon
        poligon = new PolygonShape(); // tipus de poligon
        poligon.setAsBox((WIDTH + 5) / 3, (HEIGHT - 10) / 2);  // mida del poligon
        FixtureDef fdef = new FixtureDef();//Creem la fixtureDef
        fdef.shape = poligon;//Asignem el poligon
        pumpkinFixture = pumpkinBody.createFixture(fdef);//creem la fixture a partir del body
        pumpkinFixture.setUserData(this);
        poligon.dispose();//destruim el poligon
    }

    //Comprovem en quin estat esta.
    public State getState() {
        if (pumpkinBody.getLinearVelocity().y > 0) {
            return State.JUMPING;
        } else if (pumpkinBody.getLinearVelocity().y < 0) {
            return State.FALLING;
        } else if (pumpkinBody.getLinearVelocity().x != 0) {
            return State.RUNNING;
        } else {
            return State.STANDING;
        }
    }

    public int checkDoubleJump(int doubleJump) {
        if ((Pumpkin.getCurrentState().equals(State.RUNNING) && pumpkinBody.getGravityScale()==50)|| Pumpkin.getCurrentState().equals(State.STANDING) && pumpkinBody.getGravityScale()==50) {
            return 2;
        } else {
            return doubleJump;
        }
    }


    public void init(float posX, float posY) {
        doubleJump=2;
        this.initPosX=posX;
        this.initPosY=posY;
        setCurrentState(Pumpkin.State.STANDING);
        world.destroyBody(pumpkinBody);
        setPosition(initPosX, initPosY);
        pumpkinBody = world.createBody(crearBodyDef(new Vector2(initPosX / 2, initPosY / 2)));
        crearPoligonFixture();
        Vector2 pos = pumpkinBody.getPosition();
        pumpKinSprite.setCenter(pos.x, pos.y);
    }

    //Lliberem recursos
    public void dispose() {
        texture.dispose();
        world.destroyBody(pumpkinBody);
    }


}
